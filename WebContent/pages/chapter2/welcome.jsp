<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
  <!-- <h3>Welcome to  <%=session.getAttribute("message") %></h3>-->
  <h3>Welcome to  ${message}</h3>
  <h4>To day: <%=new Date() %></h4>
  <h4><a href='<c:url value="/chapter5/LogoutServlet"/>'> Logout</a></h4>
</body>
</html>