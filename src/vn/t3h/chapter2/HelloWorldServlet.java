package vn.t3h.chapter2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

public class HelloWorldServlet extends HttpServlet{

	
	
	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
	
		HttpServletResponse resp = (HttpServletResponse) res;
		PrintWriter out = resp.getWriter();
		out.println("<html><h1>Hello world!</h1></html>");
	}

	
	
}
