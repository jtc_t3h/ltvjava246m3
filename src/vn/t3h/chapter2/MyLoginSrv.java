package vn.t3h.chapter2;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MyLoginSrv
 */
@WebServlet(urlPatterns = "/chapter2/login.html", initParams = {@WebInitParam(name = "username", value = "admin"),
		@WebInitParam(name = "password", value = "123456")
})
public class MyLoginSrv extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyLoginSrv() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("=============== doGet =================");
		
		HttpSession session = request.getSession();
		session.setAttribute("hidden", "Xin chao");
		
		response.sendRedirect("/LTVJava246M3/pages/chapter2/login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("================ doPost ===============");
		
		// Kiem tra du lieu username va password co hop le hay khong?
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		String configUserser = getServletConfig().getInitParameter("username");
		String configPassword = getServletConfig().getInitParameter("password");
		
		String hidden = request.getParameter("hidden");
		
//		if (!username.isEmpty() && !password.isEmpty() && username.equalsIgnoreCase(password)) {
		if (!username.isEmpty() && !password.isEmpty() && username.equals(configUserser) && password.equals(configPassword)) {
			// successful
			HttpSession session = request.getSession();
			session.setAttribute("message", username); // container -> attributeAdded (MySessionAttributeListener)
			
			response.sendRedirect("/LTVJava246M3/pages/chapter2/welcome.jsp");
		} else {
			// Fail
			String message = "username or password wrong.";
			request.setAttribute("message", message);
			
			request.getRequestDispatcher("/pages/chapter2/login.jsp").forward(request, response);
		}
	}

}
