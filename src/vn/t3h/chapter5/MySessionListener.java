package vn.t3h.chapter5;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class MySessionListener implements HttpSessionListener{

	private static int counter = 0;
	
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		System.out.println("counter = " + (++counter));
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		System.out.println("counter = " + (--counter));
	}

	
}
