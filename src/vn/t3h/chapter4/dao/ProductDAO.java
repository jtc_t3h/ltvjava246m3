package vn.t3h.chapter4.dao;

import java.util.List;

import vn.t3h.chapter4.domain.Product;

public class ProductDAO {

	/**
	 * Tim danh sach cac dong du lieu trong bang Product
	 * @param startIdxAndLength
	 * 			startIdx: offset - diem bat dau cua du lieu
	 * 			Length: so luong dong du lieu tra ve
	 * @return
	 */
	public List<Product> find(int ... startIdxAndLength){
		String sql = null;
		if (startIdxAndLength.length == 2) {
			sql = "select * from product limit ?, ?";
		} else {
			sql = "select * from product";
		}
		
		
		// Connect DB
		// execute data
		// close DB
		
		return null;
	}
	
	public int count() {
		String sql = "select count(*) from product";
		
		// Connect DB
				// execute data
				// close DB
		return 0;
	}
	
	public List<Product> search(String title, int ... startIdxAndLength){
		String sql = null;
		if (startIdxAndLength.length == 2) {
			sql = "select * from product where title like '%?%' limit ?, ?";
		} else {
			sql = "select * from product where title like '%?%'";
		}
		
		
		// Connect DB
		// execute data
		// close DB
		
		return null;
	}
}
