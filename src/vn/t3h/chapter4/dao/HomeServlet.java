package vn.t3h.chapter4.dao;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter4.domain.Product;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/chapter4/home.html")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ProductDAO dao = new ProductDAO();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int p = 1;
		if (request.getParameter("p") != null) {
			p = Integer.parseInt(request.getParameter("p"));
		}
		
		// lay danh sach product tu database
		List<Product> list = dao.find(p, 8);
//		int count = dao.find().size();
		int count = dao.count();
		
		String title = request.getParameter("q");
		List<Product> list2 = dao.search(title,p, 8);
		int count2 = dao.search(title).size();
		
		request.setAttribute("n", count); 
		request.setAttribute("list", list);
		
		request.setAttribute("n", count2); 
		request.setAttribute("list", list2);
		
		// Chuyen sang trang home.jsp
		request.getRequestDispatcher("").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
